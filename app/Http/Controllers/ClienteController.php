<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cliente;
use App\Http\Requests\ClienteFormRequest;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $dados['clientes'] = Cliente::paginate(10);
        } catch (\Exception $exception) {
            //dd($exception);
            return redirect()->back()->withErrors('Não foi possível listar os Clientes');
        }

        return view('cliente.index', $dados);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cliente.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClienteFormRequest $request)
    {
        try {
            $values = $request->validated();
            Cliente::create($values);
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors('Não foi possível criar um Cliente');
        }

        return redirect()->route('cliente.index')->with('reponse', 'Sucesso ao criar o Cliente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $dados['cliente'] = Cliente::findOrFail($id);
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors('Não foi possível deletar o Cliente');
        }

        return view('cliente.update', $dados);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClienteFormRequest $request, $id)
    {
        try {
            $dat = (object)$request->validated();
            $enquete = Cliente::findOrFail($id);
            $enquete->fill(['nome' => $dat->nome]);
            $enquete->fill(['sexo' => $dat->sexo]);
            $enquete->fill(['data_nascimento' => $dat->data_nascimento]);
            $enquete->fill(['cep' => $dat->cep]);
            $enquete->fill(['endereco' => $dat->endereco]);
            $enquete->fill(['numero' => $dat->numero]);
            $enquete->fill(['bairro' => $dat->bairro]);
            $enquete->fill(['cidade' => $dat->cidade]);
            $enquete->fill(['estado' => $dat->estado]);
            $enquete->save();
        } catch (\Exception $exception) {
            dd($exception);
            return redirect()->back()->withErrors('Não foi possível criar um Cliente');
        }

        return redirect()->route('cliente.index')->with('reponse', 'Sucesso ao criar o Cliente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $cliente = Cliente::findOrFail($id);
            $cliente->delete();
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors('Não foi possível deletar o Cliente');
        }

        return redirect()->route('cliente.index')->with('reponse', 'Sucesso ao deletar o Cliente');
    }
}
