<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClienteFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nome' => 'required|string|min:2|max:255',
            'sexo' => 'required|string|max:255',
            'data_nascimento' => 'required|string',
            'cep' => 'nullable|string|max:255',
            'endereco' => 'nullable|string|max:255',
            'numero' => 'nullable|string|max:255',
            'bairro' => 'nullable|string|max:255',
            'cidade' => 'nullable|string|max:255',
            'estado' => 'nullable|string|max:255',
        ];
        return $rules;
    }

    public function messages()
    {
        return [
            'nome.required' => 'Campo nome é obrigatório.',
            'sexo.required' => 'Campo sexo é obrigatório.',
            'data_nascimento.required' => 'Campo data de nascimento é obrigatório.',
        ];
    }
}
