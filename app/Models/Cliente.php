<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use HasFactory;
    protected $table = 'clientes';

    protected $fillable = [
        'id',
        'nome',
        'data_nascimento',
        'sexo',
        'cep',
        'endereco',
        'numero',
        'bairro',
        'cidade',
        'estado'
     ];

     public function setDataNascimentoAttribute($valor){
        $this->attributes['data_nascimento'] = date('Y-m-d', strtotime($valor));  
    }

    public function getDataNascimentoAttribute($value)
    {
        return date('d/m/Y', strtotime($value));
    }
}
