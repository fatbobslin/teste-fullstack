(function () {
    'use strict';
    window.addEventListener('load', function () {
        // Pega todos os formulários que nós queremos aplicar estilos de validação Bootstrap personalizados.
        var forms = document.getElementsByClassName('needs-validation');
        // Faz um loop neles e evita o envio
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                    form.classList.add('was-validated');
                    return;
                }
                setButtonLoading(form);
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();

function setButtonLoading(form) {
    var formId = form.id;

    $('#' + formId + ' button[type="submit"]').tooltip('hide');

    $('#' + formId + ' button[type="submit"]').html("Processando...").attr('disabled', 'true');
    $('#' + formId + ' :input').attr("readonly", true);
}
