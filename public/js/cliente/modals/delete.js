$('#clienteDelete').on('show.bs.modal', function (event) {
    
    let button = $(event.relatedTarget);
    let id = button.data('cliente-id');
    let nome = button.data('cliente-nome');
    let modal = $(this);

    //modal.find('#dcliente-nome').html(nome);
    document.getElementById("dcliente-nome").innerHTML = nome;
    $('#clienteDeleteForm').attr('action', `/cliente/${id}`) ;
});
