$('#clienteUpdate').on('show.bs.modal', function (event) {
    let button = $(event.relatedTarget);
    let id = button.data('cliente-fisico-id');
    let tipo = button.data('cliente-fisico-tipo');
    let nome = button.data('cliente-fisico-nome');
    let email = button.data('cliente-fisico-email');
    let cpf = button.data('cliente-fisico-cpf');
    let cep = button.data('cliente-fisico-cep');
    let endereco = button.data('cliente-fisico-logradouro');
    let bairro = button.data('cliente-fisico-bairro');
    let numero = button.data('cliente-fisico-numero');
    let cidade = button.data('cliente-fisico-cidade');
    let estado = button.data('cliente-fisico-estado');
    let telefone = button.data('cliente-fisico-telefone');
    let celular = button.data('cliente-fisico-celular');
    let instagram = button.data('cliente-fisico-instagram');
    let facebook = button.data('cliente-fisico-facebook');

    let modal = $(this);

    //document.getElementById("ucliente-fisico-tipo").value = tipo;
    if(nome)
    document.getElementById("ucliente-fisico-nome").value = nome;
    if(email)
    document.getElementById("ucliente-fisico-email").value = email;
    if(cpf)
    document.getElementById("ucliente-fisico-cpf").value = cpf;
    if(cep)
    document.getElementById("ucliente-fisico-cep").value = cep;
    if(endereco)
    document.getElementById("ucliente-fisico-logradouro").value = endereco;
    if(bairro)
    document.getElementById("ucliente-fisico-bairro").value = bairro;
    if(numero)
    document.getElementById("ucliente-fisico-numero").value = numero;
    if(cidade)
    document.getElementById("ucliente-fisico-cidade").value = cidade;
    if(estado)
    document.getElementById("ucliente-fisico-estado").value = estado;
    if(telefone)
    document.getElementById("ucliente-fisico-telefone").value = telefone;
    if(celular)
    document.getElementById("ucliente-fisico-celular").value = celular;
    if(telefone)
        document.getElementById("ucliente-fisico-instagram").value = instagram;
    if(celular)
        document.getElementById("ucliente-fisico-facebook").value = facebook;

    $('#clienteFisicoUpdateForm').attr('action', `/admin/client/${id}`);
});
