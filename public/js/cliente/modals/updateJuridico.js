$('#pessoaJuridicoUpdate').on('show.bs.modal', function (event) {
    let button = $(event.relatedTarget);
    let id = button.data('cliente-juridico-id');
    let tipo = button.data('cliente-juridico-tipo');
    let razao_social = button.data('cliente-juridico-razao-social');
    let nome = button.data('cliente-juridico-nome');
    let email = button.data('cliente-juridico-email');
    let cnpj = button.data('cliente-juridico-cnpj');
    let cep = button.data('cliente-juridico-cep');
    let endereco = button.data('cliente-juridico-logradouro');
    let bairro = button.data('cliente-juridico-bairro');
    let numero = button.data('cliente-juridico-numero');
    let cidade = button.data('cliente-juridico-cidade');
    let estado = button.data('cliente-juridico-estado');

    let modal = $(this);

    //document.getElementById("upessoa-juridico-tipo").value = tipo;
    if(razao_social)
    document.getElementById("upessoa-juridico-razao-social").value = razao_social;
    if(nome)
    document.getElementById("upessoa-juridico-nome").value = nome;
    if(email)
    document.getElementById("upessoa-juridico-email").value = email;
    if(cnpj)
    document.getElementById("upessoa-juridico-cnpj").value = cnpj;
    if(cep)
    document.getElementById("upessoa-juridico-cep").value = cep;
    if(endereco)
    document.getElementById("upessoa-juridico-logradouro").value = endereco;
    if(bairro)
    document.getElementById("upessoa-juridico-bairro").value = bairro;
    if(numero)
    document.getElementById("upessoa-juridico-numero").value = numero;
    if(cidade)
    document.getElementById("upessoa-juridico-cidade").value = cidade;
    if(estado)
    document.getElementById("upessoa-juridico-estado").value = estado;

    $('#pessoaJuridicoUpdateForm').attr('action', `/admin/pessoa/${id}`);
});
