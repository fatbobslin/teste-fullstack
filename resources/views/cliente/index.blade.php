@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Lista de Clientes
                </div>

                <div class="card-body">
                    <div class="box-cad float-right">
                        <a href="{{route('cliente.create')}}" title="Cadastrar" class="btn btn-success">Cadastrar</a>
                    </div>
                    <table class="table">
                        <thead>
                            <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nome</th>
                            <th scope="col">Data de Nascimento</th>
                            <th scope="col">Sexo</th>
                            <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($clientes as $cliente)
                                <tr>
                                    <th scope="row">{{$cliente->id}}</th>
                                    <td>{{$cliente->nome}}</td>
                                    <td>{{$cliente->data_nascimento}}</td>
                                    <td>{{$cliente->sexo == 'M' ? 'Masculino' : 'Feminino'}}</td>
                                    <td>
                                        <a href="{{route('cliente.edit', $cliente->id)}}" title="Alterar" class="btn btn-primary btn-sm">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <button data-toggle="modal" data-target="#clienteDelete"
                                                data-cliente-id="{{$cliente->id}}"
                                                data-cliente-nome="{{$cliente->nome}}"
                                                class="btn btn-danger btn-sm" type="button" title="Deletar">
                                            <i class="fas fa-trash-alt"></i>
                                        </button>
                                    </td>
                                </tr>
                            @empty
                                <tr style="text-align: center">
                                    <td colspan="4">Nenhum registro cadastrado no momento.</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                    <div class="box-pagination">{{$clientes->links("pagination::bootstrap-4")}}</div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('cliente.modals.delete')

@endsection


@section('js')
    <script src="{{ asset('js/cliente/modals/delete.js') }}"></script>
@endsection
