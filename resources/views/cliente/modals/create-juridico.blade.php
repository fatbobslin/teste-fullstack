<div class="modal fade" id="pessoaJuridicoCreate">
    <form method="POST" id="pessoaJuridicoCreateForm" action="{{route('cliente.store')}}" enctype="multipart/form-data" class="needs-validation" autocomplete="off" novalidate>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Cadastrar Cliente Jurídico</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    @csrf
                    <input name="tipo" type="hidden" value="Jurídico" />
                    <div class="form-group">
                        <label>Razao Social</label>
                        <input value="{{old('razao_social')}}" type="text" name="razao_social" required
                               class="form-control @error('razao_social') is-invalid @enderror">
                        <div class="invalid-feedback">Obrigatório</div>
                    </div>
                    <div class="form-group">
                        <label>Nome Fantasia</label>
                        <input value="{{old('nome')}}" type="text" name="nome" required
                               class="form-control @error('nome') is-invalid @enderror">
                        <div class="invalid-feedback">Obrigatório</div>
                    </div>
                    <div class="form-group">
                        <label>E-mail</label>
                        <input value="{{old('email')}}" type="email" name="email" required
                               class="form-control @error('email') is-invalid @enderror">
                        <div class="invalid-feedback">Obrigatório</div>
                    </div>
                    <div class="form-group">
                        <label>CNPJ</label>
                        <input value="{{old('cnpj')}}" type="text" name="cnpj" required
                               class="form-control cnpj @error('cnpj') is-invalid @enderror">
                        <div class="invalid-feedback">Obrigatório</div>
                    </div>
                    <div class="form-group">
                        <label>CEP</label>
                        <input value="{{old('cep')}}" type="text" name="cep" required
                               class="form-control cep @error('cep') is-invalid @enderror">
                        <div class="invalid-feedback">Obrigatório</div>
                    </div>
                    <div class="form-group">
                        <label>Endereço</label>
                        <input value="{{old('logradouro')}}" type="text" name="logradouro"
                               class="form-control logradouro @error('logradouro') is-invalid @enderror">
                    </div>
                    <div class="form-group">
                        <label>Número</label>
                        <input value="{{old('numero')}}" type="text" name="numero"
                               class="form-control @error('numero') is-invalid @enderror">
                    </div>
                    <div class="form-group">
                        <label>Bairro</label>
                        <input value="{{old('bairro')}}" type="text" name="bairro"
                               class="form-control bairro @error('bairro') is-invalid @enderror">
                    </div>
                    <div class="form-group">
                        <label>Cidade</label>
                        <input value="{{old('cidade')}}" type="text" name="cidade"
                               class="form-control cidade @error('cidade') is-invalid @enderror">
                    </div>
                    <div class="form-group">
                        <label>Estado</label>
                        <input value="{{old('estado')}}" type="text" name="estado"
                               class="form-control uf @error('estado') is-invalid @enderror">
                    </div>
                    <div class="form-group @error('image') has-error @enderror">
                        <label for="exampleInputFile">Imagem</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" name="image" class="custom-file-input" id="exampleInputFile">
                                <label class="custom-file-label" for="exampleInputFile">Selecione</label>
                            </div>
                        </div>
                        <a href="" class="box-img" id="uempresa-a-image" data-toggle="lightbox" data-gallery="example-gallery" title="Visualizar" >
                            <img src="" id="uempresa-image" class="img-fluid" width="100" />
                        </a>
                    </div>

                </div>
                <div class="modal-footer justify-content-between">
                    <button type="submit" class="btn btn-success btn-sm"><i class="fas fa-plus"></i> Cadastrar</button>
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </form>
    <!-- /.modal-dialog -->
</div>
