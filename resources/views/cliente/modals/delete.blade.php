<div class="modal fade" id="clienteDelete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="clienteDeleteForm" action="" method="POST" class="needs-validation" novalidate>
                @csrf
                @method("DELETE")

                <div class="modal-header card-header-danger">
                    <h4 class="modal-title">Deletar Cliente</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Você tem certeza que deseja deletar o Cliente <span class="font-weight-bold text-danger" id="dcliente-nome"></span>?
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="submit" class="btn btn-danger btn-sm">Deletar</button>
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancelar</button>
                </div>
            </form>
        </div>
    </div>
</div>

