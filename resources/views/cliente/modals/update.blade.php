<div class="modal fade" id="clienteUpdate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="clienteFisicoUpdateForm" action="" method="POST" enctype="multipart/form-data" class="needs-validation" novalidate>
                <div class="modal-header">
                    <h4 class="modal-title">Alterar Cliente Físico</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    @csrf
                    @method("PATCH")
                    <input name="tipo" type="hidden" value="Físico" />
                    <div class="form-group">
                        <label>Nome</label>
                        <input value="{{old('nome')}}" id="ucliente-fisico-nome" type="text" name="nome" required
                               class="form-control @error('nome') is-invalid @enderror">
                        <div class="invalid-feedback">Obrigatório</div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>E-mail</label>
                                <input value="{{old('email')}}" id="ucliente-fisico-email" type="email" name="email" required
                                       class="form-control @error('email') is-invalid @enderror">
                                <div class="invalid-feedback">Obrigatório</div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>CPF</label>
                                <input value="{{old('cpf')}}" id="ucliente-fisico-cpf" type="text" name="cpf" required
                                       class="form-control cpf @error('cpf') is-invalid @enderror">
                                <div class="invalid-feedback">Obrigatório</div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>CEP</label>
                        <input value="{{old('cep')}}" id="ucliente-fisico-cep" type="text" name="cep" required
                               class="form-control cep @error('cep') is-invalid @enderror">
                        <div class="invalid-feedback">Obrigatório</div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Endereço</label>
                                <input value="{{old('logradouro')}}" id="ucliente-fisico-logradouro" type="text" name="logradouro"
                                       class="form-control logradouro @error('logradouro') is-invalid @enderror">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Número</label>
                                <input value="{{old('numero')}}" id="ucliente-fisico-numero" type="text" name="numero"
                                       class="form-control @error('numero') is-invalid @enderror">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Bairro</label>
                        <input value="{{old('bairro')}}" id="ucliente-fisico-bairro" type="text" name="bairro"
                               class="form-control bairro @error('bairro') is-invalid @enderror">
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Cidade</label>
                                <input value="{{old('cidade')}}" id="ucliente-fisico-cidade" type="text" name="cidade"
                                       class="form-control cidade @error('cidade') is-invalid @enderror">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Estado</label>
                                <input value="{{old('estado')}}" id="ucliente-fisico-estado" type="text" name="estado"
                                       class="form-control uf @error('estado') is-invalid @enderror">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Telefone</label>
                                <input value="{{old('telefone')}}" id="ucliente-fisico-telefone" type="text" name="telefone"
                                       class="form-control telefone @error('telefone') is-invalid @enderror">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><i class="fab fa-whatsapp"></i> Celular</label>
                                <input value="{{old('celular')}}" id="ucliente-fisico-celular" type="text" name="celular"
                                       class="form-control celular @error('celular') is-invalid @enderror">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><i class="fab fa-instagram"></i> Instagram</label>
                                <input value="{{old('instagram')}}" id="ucliente-fisico-instagram" type="text" name="instagram"
                                       class="form-control @error('instagram') is-invalid @enderror">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><i class="fab fa-facebook"></i> Facebook</label>
                                <input value="{{old('facebook')}}" id="ucliente-fisico-facebook" type="text" name="facebook"
                                       class="form-control @error('facebook') is-invalid @enderror">
                            </div>
                        </div>
                    </div>
                    <!--<div class="form-group @error('image') has-error @enderror">
                        <label for="exampleInputFile">Imagem</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" name="image" class="custom-file-input" id="exampleInputFile">
                                <label class="custom-file-label" for="exampleInputFile">Selecione</label>
                            </div>
                        </div>
                        <a href="" class="box-img" id="uempresa-a-image" data-toggle="lightbox" data-gallery="example-gallery" title="Visualizar" >
                            <img src="" id="uempresa-image" class="img-fluid" width="100" />
                        </a>
                    </div>-->

                </div>
                <div class="modal-footer justify-content-between">
                    <button type="submit" class="btn btn-primary btn-sm">Alterar</button>
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancelar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
