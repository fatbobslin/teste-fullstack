@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Alterar Cliente
                    <a href="{{route('cliente.index')}}" title="Voltar" class="btn btn-primary btn-sm float-right"><i class="fas fa-arrow-left"></i> </a>&nbsp;
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('cliente.update', $cliente->id) }}" class="needs-validation" autocomplete="off" novalidate>
                        @csrf
                        @method("PATCH")

                        {!!Form::text('nome', 'Nome')->value($cliente->nome)!!}
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                {!!Form::text('data_nascimento', 'Data de Nascimento')
                                    ->value($cliente->data_nascimento)
                                    ->attrs(['class' => 'data_nascimento'])!!}
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                {!!Form::select('sexo', 'Sexo', ['' => 'Selecione um sexo', 'M' => 'Masculino', 'F' => 'Feminino'])
                                    ->value($cliente->sexo)!!}
                            </div>
                        </div>
                        {!!Form::text('cep', 'CEP')
                            ->value($cliente->cep)
                            ->attrs(['class' => 'cep'])
                            ->required(false) !!}
                        <div class="row">
                            <div class="col-lg-8 col-md-6 col-sm-12 col-xs-12">
                            {!!Form::text('endereco', 'Endereço')
                                ->value($cliente->endereco)
                                ->attrs(['class' => 'logradouro'])
                                ->required(false)!!}
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                {!!Form::text('numero', 'Número')
                                    ->value($cliente->numero)
                                    ->required(false)!!}
                            </div>
                        </div>
                        {!!Form::text('bairro', 'Bairro')
                            ->value($cliente->bairro)
                            ->attrs(['class' => 'bairro'])
                            ->required(false)!!}
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                {!!Form::text('cidade', 'Cidade')
                                    ->value($cliente->cidade)
                                    ->attrs(['class' => 'cidade'])
                                    ->required(false)!!}
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                {!!Form::text('estado', 'Estado')
                                    ->value($cliente->estado)
                                    ->attrs(['class' => 'uf'])
                                    ->required(false)!!}
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary float-right">
                                Salvar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <!--<script src="{{asset('js/base.js')}}"></script>-->
    <script src="{{ asset('js/jquery.mask.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.cep').mask('00000-000');
            $('.data_nascimento').mask('00/00/0000');
        });
    </script>
    <script src="{{ asset('js/buscaCep.js') }}"></script>
@endsection