<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', [App\Http\Controllers\ClienteController::class, 'index'])->name('index');

/** Rotas de clientes **/
Route::group(['prefix' => 'cliente', 'as' => 'cliente.'], function () {
    Route::get('/', [App\Http\Controllers\ClienteController::class, 'index'])->name('index');
    Route::post('/', [App\Http\Controllers\ClienteController::class, 'store'])->name('store');
    Route::get('/create', [App\Http\Controllers\ClienteController::class, 'create'])->name('create');
    Route::get('/{id}', [App\Http\Controllers\ClienteController::class, 'show'])->name('show');
    Route::get('/{noticia}/edit', [App\Http\Controllers\ClienteController::class, 'edit'])->name('edit');
    Route::patch('/{id}', [App\Http\Controllers\ClienteController::class, 'update'])->name('update');
    Route::delete('/{id}', [App\Http\Controllers\ClienteController::class, 'destroy'])->name('destroy');
});
